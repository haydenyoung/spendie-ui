# Spendie DApp Development Test Harness

The Spendie DApp Development Test Harness has been specifically designed for use with contracts being developed locally.

This app is not designed to be developed and extended for production use. Rather, it provides a method for early testing of smart contracts deployed locally as well as a platform for building out development ideas that may not be ready for production environments. It also provides methods for manipulating the local blockchain directly, such as manually mining blocks and adjusting the latest block timestamp. This can be useful for block height/time-sensitive tests that can be performed on functions which may require the blockchain being moved forward by weeks before they can be fully evaluated.

## Installation

Clone this repository and install required dependencies:

```
git clone
npm ci
```

## Configuration

Running this DApp requires a local blockchain to also be running. You will also need to copy the smart contract artifacts from the Core repo before launching the test harness.

Clone the Core repo. You can follow the instructions in the Core's README.

Once cloned, launch a local Hardhat node from within your local Core repo. This should also compile and deploy the contracts so they are ready for use.

Once launched, you will need to create a symlink to the generated artifacts and deployment definitions:

```
cd /path/to/dapp-development/src
ln -s ../../core/artifacts ./
ln -s ../../core/deployments ./
```

The above commands rely on the following directory structure:

```
Development
├── Spendie
│   ├── core
│   ├── dapp-development
```

Alternatively, you can copy the artifacts and deployments directories from the core repo although you will need to remember to update your copy if you recompile or redeploy the contracts.

Once completed, the DApp is ready to run.

## Running the UI

To run the Spendie Development DApp, use the npm target "serve". This will launch a local instance which will update on any page changes:

```
npm run serve
```

If configured correctly, the DApp should provide information about running contracts, including the addresses of the Treasury Factory, the Treasury, Issuance and Redemption contracts as well as information about the Spendie Vol and Gov tokens.

To fully test the environment, though, you will need to add a stable token and update the SpendieVol liquidity pool's time-weight average price (TWAP) oracle contract.

In order for the DApp to run these features, you will need to propose, cast a vote on and execute contract function calls which add a stable coin and deploy a liquidity pool. The only way to accomplish this with your locally running blockchain is to use the With [Tally Governance Test Harness](https://gitlab.com/hyprojects/withtally/withtally-test-harness). Using the Governance test harness is not covered here but you can learn more about it via its [README](https://gitlab.com/hyprojects/withtally/withtally-test-harness/-/blob/main/README.md).

## Building and Deploying

You can create a deployable version of the DApp using the build target. Once built, you can deploy the test harness to other infrastructure such as a continuous integration server or IPFS.

```
npm run build
```

## Linting and Formatting

Keep code well formatted and free from issues by running the lint target:

```
npm run lint
```
