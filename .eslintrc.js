module.exports = {
  root: true,

  env: {
    node: true
  },

  plugins: ["prettier"],

  parserOptions: {
    parser: "@typescript-eslint/parser"
  },

  rules: {
    "prettier/prettier": ["error"]
  },

  ignorePatterns: ["constants/contracts/artifacts/**/*.js"],

  extends: [
    "plugin:vue/essential",
    "eslint:recommended",
    "prettier",
    "@vue/typescript"
  ]
};
