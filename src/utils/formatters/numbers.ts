import { BigNumber } from "bignumber.js";

export const decimalize = (
  amount: any,
  decimals: any,
  fix = Number(decimals.toString())
) => {
  const pow = new BigNumber(10).pow(decimals);
  const formatted = new BigNumber(amount).div(pow).toFixed(fix);

  return formatted;
};

export const integerize = (amount: any, decimals: any) => {
  const pow = new BigNumber(10).pow(decimals);
  const formatted = new BigNumber(amount).times(pow);

  return formatted;
};
