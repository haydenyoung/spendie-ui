import { BigNumber } from "bignumber.js";
import { integerize, decimalize } from "@/utils/formatters/numbers";

export const reverseRateI = (rate: any, decimals: number) => {
  const numerator = integerize(1, decimals * 2);
  return new BigNumber(numerator).div(rate).toFixed(0);
};

export const getAmountOut = (amountIn: any, rate: any, decimals: number) => {
  return decimalize(new BigNumber(amountIn).times(rate), decimals);
};
